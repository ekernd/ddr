

<h3>Add/Edit User</h3>
<form method="POST" action="<?php echo($_SERVER['PHP_SELF'])?>" enctype="multipart/form-data">
	
	<input type="hidden" name="txtId" id="txtId" value="<?php echo($user['user_id']); ?>" /><br>
	
	First Name: <input type="text" name="txtFistName" id="txtFistName" value="<?php echo($user['user_first_name']); ?>"/>
	<?php echo(isset($error_messages['user_first_name']) ? $error_messages['user_first_name'] : "" ); ?>
	<br>

	Last Name: <input type="text" name="txtLastName" id="txtLastName" value="<?php echo($user['user_last_name']); ?>"/>
	<?php echo(isset($error_messages['user_last_name']) ? $error_messages['user_last_name'] : "" ); ?>
	<br>

	Email: <input type="text" name="txtEmail" id="txtEmail" value="<?php echo($user['user_email']); ?>"/>
	<?php echo(isset($error_messages['user_email']) ? $error_messages['user_email'] : "" ); ?>
	<br>
	
	Password: <input type="password" name="txtPassword" id="txtPassword" value="<?php echo($user['user_password']); ?>"/>
	<?php echo(isset($error_messages['user_password']) ? $error_messages['user_password'] : "" ); ?>
	<br>
	
	Confirm Password: <input type="password" name="txtConfirmPassword" id="txtConfirmPassword" value="<?php echo($user['user_confirm_password']); ?>"/>
	<?php echo(isset($error_messages['user_confirm_password']) ? $error_messages['user_confirm_password'] : "" ); ?>
	<br>
	
	Role: 
	<?php

	$roles = $user_da->get_user_roles();

	$select_html = '<select id="selRole" name="selRole">';
	foreach($roles as $r){
		// check to see if we need set the option as selected
		$selected_attr = ($r['user_role_id'] == $user['user_role'] ? " selected " : "");
		$select_html .= '<option value="' . $r['user_role_id'] .'" '. $selected_attr . '>' . $r['user_role_name'] . '</option>';
	}
	$select_html .= "</select>";
	echo($select_html);

	echo(isset($error_messages['user_role']) ? $error_messages['user_role'] : "" ); 
	?>
	<br>
	
	Active: 
	<input type="radio" name="rgActive" value="yes" <?php echo( $user['user_active'] == "yes" ? "checked" : "" ); ?> /> Yes 
	<input type="radio" name="rgActive" value="no" <?php echo( $user['user_active'] == "no" ? "checked" : "" ); ?> /> No
	<br>

	Picture:
	<br>
	<?php
	if(empty($user['user_image']) === FALSE){
		echo('<img id="userImg" src="' . $upload_dir . $user['user_image'] . '" />');
	}
	?>
	<br>
	<input type="hidden" name="txtUserImage" value="<?php echo($user['user_image']); ?>" />
	<input type="file" id="picture_upload" name="picture_upload" />
	<br>
	<?php echo(isset($error_messages['picture_upload']) ? $error_messages['picture_upload'] : "" ); ?>
	<br>


	<input type="submit" class="home" name="btnSubmit" value="submit" />
</form>

<script>
// THIS IS TEMPORARY, But if you choose an image to upload,
// then it will clear out the existing image from being displayed.
document.getElementById("picture_upload").onchange = function(){
	var img = document.getElementById("userImg");
	if(img){
		img.src = "";
	}
};
</script>