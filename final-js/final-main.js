window.addEventListener("load", function(){

    var navButton = document.getElementById("nav-button");
    var navBar = document.getElementById("nav-bar");

    navButton.addEventListener('click', function(){

        var navBarWidth = window.getComputedStyle(navBar).getPropertyValue("width");
        navBarWidth = parseInt(navBarWidth);
        
        if(navBarWidth == 0){
            navBar.style.width = "200px";
        }else if(navBarWidth == 200){
            navBar.style.width = "0px";
        }
    });

});

window.onload = function(){
        var form = document.getElementById("frmContact");
        form.onsubmit = validate;
    };
    function validate(){
        var isValid = true;
        var txtFirstName = document.getElementById("txtFirstName")

        if(txtFirstName.value == ""){
            isValid = false;
            alert("You must enter your First name.");
            txtFirstName.style.display = "block";
        };
        var txtLastName = document.getElementById("txtLastName")
        
        if(txtLastName.value == ""){
            isValid = false;
            alert("You must enter your Last name.");
            txtLastName.style.display = "block";
        };

        var txtEmail = document.getElementById("txtEmail")
        
        if(txtEmail.value == ""){
            isValid = false;
            alert("You need to put in an email.")
            txtEmail.innerHTML = "You must enter an email address.";
            txtEmail.style.display = "block";
        }else if(validateEmail(txtEmail.value) == false){
            isValid = false; 
            txtEmail.innerHTML = "Your email address isn't valid";
            txtEmail.style.display = "block";
        };

        var txtComments = document.getElementById("txtComments")

        if(txtComments.value == ""){
            isValid = false;
            txtComments.innerHTML = "You must enter a comment";
            txtComments.style.display = "block";
        };



        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        };

        
        return isValid; 

    };
// assume that there is a div tag on the page with an id set to 'someDiv'

// function clickEventHandler(){
//     alert("You clicked!");
//     return "blah!";
// }
// div.onclick
// var div = document.getElementById("someDiv");
// div.onclick = clickEventHandler();