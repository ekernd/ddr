<?php 
  require_once("includes/config.inc.php");
  
 ?>
 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php echo($page_title); ?></title>
  <meta name="description" content="THIS IS BECOMING MORE IMPORTANT">
  <meta name="viewport" content="width=device-width">
  <!-- link had a "/" in from of it.  the reset was not loading -->
  <!-- <link REL=StyleSheet type="text/css" href="final-css/reset.css">   -->
  <link REL=StyleSheet HREF="<?php echo(ROOT_DIR); ?>final-css/reset.css"> 
  <link REL=StyleSheet HREF="<?php echo(ROOT_DIR); ?>final-css/style.css">
  <!-- <script type="text/javascript" src="final-js/final-main.js"></script>  not sure why this js file needs to be here.  I think it only needs to be in the breat new user page -->
</head>
<body>
<div id="page-wrap">  
    <div id="banner">
      
      <div id="header-global">
        <img src="logo-clear.png" class="logo-header"><h1>Focus a DDR product</h1>
        <!-- This logo must be cropped -->
        
      </div>

      
    </div>
    <!-- <div id="nav-button">&#9776;</div> -->
    <div id="content">
  <nav id="menu" class="clearfix">
     <div class="InputAddOn">
         <input class="InputAddOn-field" style= "width: 200px"; placeholder="Search by user, tag, or date">
         <a class="button-search" href="#">Search</a>
         <!-- <button class="InputAddOn-item">Search</button> -->
      </div>

    <ul>
      <!-- <a href="<?php echo(ROOT_DIR); ?>index.php">Home</a> -->
      <a class="button-home" href="<?php echo(ROOT_DIR); ?>index.php">Home</a>
      <a class="button-uploadPhoto" href="<?php echo(ROOT_DIR); ?>newImageUpload.php">Upload a New Photo</a>
      <!-- <a href="<?php echo(ROOT_DIR); ?>newImageUpload.php">Upload A New Photo</a> -->
      <a class="button-logout" href="login.php">Log out</a>
      <!-- <a href="login.php">Log Out</a> -->
    </ul>
  </nav> 