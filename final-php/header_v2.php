<?php 
  require_once("includes/config.inc.php");
  
 ?>
 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title><?php echo($page_title); ?></title>
  <meta name="description" content="THIS IS BECOMING MORE IMPORTANT">
  <meta name="viewport" content="width=device-width">
  <!-- link had a "/" in from of it.  the reset was not loading -->
  <!-- <link REL=StyleSheet type="text/css" href="final-css/reset.css">   -->
  <link REL=StyleSheet HREF="<?php echo(ROOT_DIR); ?>final-css/reset.css"> 
  <link REL=StyleSheet HREF="<?php echo(ROOT_DIR); ?>final-css/style_v2.css">
  
  <script type="text/javascript" src="final-js/header.js"></script>  
</head>
<body>
<div id = 'top'> 
    <header>
      
      <div id="header-global">
        <img src="logo-clear.png" class="logo-header"><h1>Focus a DDR product</h1>
        <!-- This logo must be cropped -->
        
      </div>

      <div id="mobile-nav-button">&#9776;</div>
     </header>
    <!-- <div id="nav-button">&#9776;</div> -->
   
 <nav id = "main-nav">
    

    <ul>
      <!-- <a href="<?php echo(ROOT_DIR); ?>index.php">Home</a> -->
     <li> <a class="button-home" href="<?php echo(ROOT_DIR); ?>index.php">Home</a></li>
     <li> <a class="button-uploadPhoto" href="<?php echo(ROOT_DIR); ?>newImageUpload.php">Upload a New Photo</a></li>
      <!-- <a href="<?php echo(ROOT_DIR); ?>newImageUpload.php">Upload A New Photo</a> -->
     <li> <a class="button-logout" href="login.php">Log out</a></li>
      <!-- <a href="login.php">Log Out</a> -->
      <li>  
        <ul>
             <li>  <input class="InputAddOn-field" style= "width: 200px"; placeholder="Search by user, tag, or date"></li>
        <li>   <a class="button-search" href="#" >Search</a></li>
        </ul>
        
         <!-- <button class="InputAddOn-item">Search</button> -->
       
    

    </li>
    </ul>
  </nav> 
</div>  
    <!-- beginning of main body content div -->
    <div id="content">