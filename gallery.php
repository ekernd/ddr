<?php
$page_title= 'DDR Gallery';
include("includes/config.inc.php");
require("includes/dataaccess/UserDataAccess.php");
require_once("includes/dataaccess/FileDataAccess.php");
require_once("final-php/header_v2.php");
$_SESSION['selectedGallery'] = $_GET['galleryID'];
$user_first_name =  $_SESSION['user_first_name'];
///var_dump($_SESSION);

$link = get_link();
$file_da = new FileDataAccess($link);
$user_da = new UserDataAccess($link);
$photoIdandExtention = array();
$galleryPhoto = array();
$thePhotoIdsInGallery = $user_da -> get_photos_by_galleryID();
$all_photos=$user_da->get_all_photos();
 //var_dump($thePhotoIdsInGallery);

foreach ($thePhotoIdsInGallery as $photoID ) {
	$_SESSION['file_id']= $photoID['file_id'];
	$all_photos=$user_da->get_all_photos();
		foreach ($all_photos as $p) {
			# code...
		
			if($p['file_id'] == $photoID['file_id']){

					array_push($galleryPhoto, $p);
			}
		}

}

// var_dump($galleryPhoto);

$imgDescriptions = array();

foreach ($thePhotoIdsInGallery as $photoID ) {
	// get_photoDescription_byFileID()
	$_SESSION['file_id']= $photoID['file_id'];
	$description =  $file_da->get_photoDescription_byFileID();
	//echo($description);
	
		array_push($imgDescriptions, $description);

}
//var_dump($imgDescriptions);
?>
<script type="text/javascript" src="final-js/image-gallery.js"></script>

	<body>
		<script>
			
		
		
		window.addEventListener("load", function() {
			
			var images = [<?php echo '"'.implode('","', $photoIdandExtention).'"' ?>];
			console.log(images);
			var imageAlts = [<?php echo '"'.implode('","', $imgDescriptions).'"' ?>];
			console.log(imageAlts);
		
			makeGallery({
	
			images:images,
			imageAlts:imageAlts
		});
	});
		</script>
		<br>


		<main>
			

<div id="centerTheDiv">
				<h1 ><?php echo($user_first_name); ?>'s Gallery Images</h1>
				<br>
				<div id="main-image">
				</div>
				<div id="image-gallery">
				 <?php 

			


				 $rowCount = 0;
				 foreach ($galleryPhoto as $gp) {
					 $key = $gp['file_id'] .'.'. $gp['file_extension'];
					// var_dump($key);
					 $altTag = $gp['photoDescription'];
				//	 var_dump($altTag);
					$rowCount++;
					echo('<img src="uploaded-files/thumbnails/' . $key. '" alt = "'.$altTag. '" id = "'.$key.'" >');

								if ($rowCount >=5) {
									echo('<br>');
									$rowCount = 0;
								}

	
				  }
				 
				 ?>
				
				</div>
				
				

</div>

		</main>

	</body>
	<?php
require_once("final-php/footer_v2.php");
?>