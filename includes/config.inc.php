<?php

// this is the main configuration file for the website
//start session if not alread set
if(!session_id()) session_start();
define("ADMIN_ROLE", 2);
define("STANDARD_USER_ROLE", 1);
// echo '<pre>';
// var_dump($_SERVER) ;
// echo '</pre>';
// detect which environment the code is running in
if($_SERVER['SERVER_NAME'] == "localhost"){
	// DEV ENVIRONMENT SETTINGS
	// echo("we are in DEV ENVIRONMENT SETTINGS");
	define("DEBUG_MODE", true);
	define("DB_HOST", "localhost");
	define("DB_USER", "root");
	define("DB_PASSWORD", "");
	define("DB_NAME", "focus_ddr");
	define("SITE_ADMIN_EMAIL", "XXXXXXX");
	define("SITE_DOMAIN", $_SERVER['SERVER_NAME']);
	define("ROOT_DIR", "/ddr/");
	// echo("root dir is set to " . ROOT_DIR );

}else{
	// PRODUCTION SETTINGS
	define("DEBUG_MODE", true); 
//	echo("we are in production settings");
	// you may want to set DEBUG_MODE to true when you 
	// are first setting up your live site, but once you get
	// everything working you'd want it off.
	define("DB_HOST", "localhost");
	define("DB_USER", "daveeker_user");	
	define("DB_PASSWORD", "daveekern");
	define("DB_NAME", "daveeker_focus_ddr");
	define("SITE_ADMIN_EMAIL", "webmaster@daveekern.com");
	define("SITE_DOMAIN", $_SERVER['SERVER_NAME']);
	//define("ROOT_DIR", "/adv-web-dev/php/site-setup-7-file-uploads/");
	define("ROOT_DIR", "/ddr/");
	//echo("root dir is set to " . ROOT_DIR );
}


//ftp://daveeker@daveekern.com/public_html/final-photo-web-app-project
// if ($_SERVER['HTTP_HOST']=="localhost:8080") {
//     define("ROOT_DIR", "/adv-web-dev/adv-web/php/TwoPageResponsiveWebsite/");
// }else{
//     define("ROOT_DIR", "../final-photo-web-app-project/");


// }
// echo '<pre>';
// var_dump($_SERVER) ;
// echo '</pre>';
// if we are in debug mode then display all errors and set error reporting to all 
if(DEBUG_MODE){
	// turn on error messages
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
}

// the $link variable will be our connection to the database
$link = null;

function get_link(){

	global $link;
		
	if($link == null){
		
		$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

		if(!$link){
			throw new Exception(mysqli_connect_error()); 
		}
	}
	
	return $link;
	
}
echo($link);

// set up custom error handling
require_once('custom_error_handler.inc.php');


