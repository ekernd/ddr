<?php
class PhotoTagsDataAccess{
	
	private $link;
	const DUPLICATE_TAG_ERROR = "That tag already exists";
	/**
	 * Constructor
	 *
	 * @param connection $link 	The link the the database 		
	 */
	function __construct($link){
		$this->link = $link;
	}
	


	/**
	* Gets all phototags
	* 
	* @return array Returns an array of photostags  
	* 				
	*/
	function get_all_photo_tags(){
		$qStr = "SELECT
					*
				FROM tblphototags" ;
		// If this query is bad, maybe the single quotes of 'yes' need to be escaped
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_photo_tags = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$photoTag = array();
			$photoTag['photoTagsID'] = htmlentities($row['photoTagsID']);
			$photoTag['tagID'] = htmlentities($row['tagID']);
			$photoTag['file_id'] = htmlentities($row['file_id']);
			

			// add the $tag to the $all_photo_tags array
			$all_photo_tags[] = $photoTag;
		}

		return $all_photo_tags;
			
	}


	/**
	* Inserts a new tag into the tags table
	*
	* @param array 		An obj/array that has the following properties: 
	*					tagID, fileID
	*
	* @return array 	Returns an assoc array, along with the new photoTagsID
	* 					Returns false if something goes wrong.
	*/
	function insert_photoTag($photoTag){

		// prevent SQL injection
		$photoTag['tagID'] = mysqli_real_escape_string($this->link, $photoTag['tagID']);
		$photoTag['file_id'] = mysqli_real_escape_string($this->link, $photoTag['file_id']);
	

		

		$qStr = "INSERT INTO tblphototags (
					tagID,
					file_id
					
				) VALUES (
					'{$photoTag['tagID']}',
					'{$photoTag['file_id']}'
					
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the photoTag id that was assigned by the data base
			$photoTag['tagID'] = mysqli_insert_id($this->link);
			// then return the photoTag
			return $photoTag;
		}else{
			$this->handle_error("unable to insert photo tag");
		}

		return false;
	}

	/**
	* Deactivates an existing tag in the tags table
	*
	* @param array 		An obj/array that has the following properties: 
	*					tagID,  tag_active
	*
	* @return array 	Returns an assoc array with all the tag properties
	* 					Returns false if something goes wrong.
	*/
	function update_tag($tag){

		// prevent SQL injection
		$tag['tagID'] = mysqli_real_escape_string($this->link, $tag['tagID']);
		//$tag['tagDescription'] = mysqli_real_escape_string($this->link, $tag['tag_first_name']);
		$tag['tag_active'] = mysqli_real_escape_string($this->link, $tag['tag_active']);
		

	

		$qStr = "UPDATE tbltags SET
			
				tag_active = '{$tag['tag_active']}'
				WHERE tagID = " . $tag['tagID'];
					
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $tag;
		}else{
			$this->handle_error("unable to update tag");
		}

		return false;
	}
// "DELETE FROM `tblphototags` WHERE `tblphototags`.`photoTagsID` = 44"?
	/**
	* Deletes an existing tag in the tblphototags table
	*
	* @param tag 		tagID
	*
	* @return array 	Returns an assoc array with all the tag properties
	* 					Returns false if something goes wrong.
	*/
	function delete_tag($tagID){

		// prevent SQL injection
		$tag['tagID'] = mysqli_real_escape_string($this->link, $tagID);
		
		

		// "DELETE FROM `tblphototags` WHERE `tblphototags`.`photoTagsID` = 44"?

		$qStr = "DELETE FROM `tblphototags`WHERE `tblphototags`.`photoTagsID` = " . $tag['tagID'];
					
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $result;
		}else{
			$this->handle_error("unable to delete tag");
		}

		return false;
	}


	/**
	* Get photoTagID and tagDescription ny file_id
	*
	* @param number 		file_id
	*
	* @return array 	Returns an assoc array with photptagId and tagDescription
	*/
	function getPhotoTagsByFileID($file_id){

		// prevent SQL injection
		$file_id = mysqli_real_escape_string($this->link, $file_id);
		

		$qStr = "SELECT photoTagsID, tagDescription
				FROM tblphototags
				INNER JOIN tbltags
				ON  tblphototags.tagID = tbltags.tagID
				WHERE  tblphototags.file_id = " . $file_id;
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		
		$tagsOnPhoto = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$photoTag = array();
			$photoTag['photoTagsID'] = htmlentities($row['photoTagsID']);
			$photoTag['tagDescription'] = htmlentities($row['tagDescription']);
			
			// add the $photoTag to the $tagsOnPhoto array
			$tagsOnPhoto[] = $photoTag;
		}

		return $tagsOnPhoto;
			
	}

	function handle_error($msg){
        //die("handle error  "  . $msg);

        // If the user being inserted already exists (same user_email) then the db will return this error msg:
        // 'Duplicate entry ...'
        // But how should we notify the user of the problem?????
        // Let's leave that up to the client code, we'll throw an error that they can catch (if they choose to do so)
        if(strpos($msg, "Duplicate entry") !== FALSE){ // I think 'Duplicate entry' is an error from the db
            throw new Exception(self::DUPLICATE_TAG_ERROR); 
        }else{
            // how do we want to handle this? should we throw an exception
            // and let our custom EXCEPTION handler deal with it?????
            $stack_trace = print_r(debug_backtrace(), true);
            throw new Exception($msg . " - " . $stack_trace);
        }
 	}
}