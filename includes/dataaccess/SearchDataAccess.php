<?php
class SearchDataAccess{
	
	private $link;

	const DUPLICATE_USER_ERROR = "That email is already in the db";
	
	/**
	 * Constructor
	 *
	 * @param connection $link 	The link the the database 		
	 */
	function __construct($link){
		$this->link = $link;
	}
	

	/**
	* Gets all users
	* 
	* @return array Returns an array of User objects??? 
	* 				Or an array of associative arrays???
	*/
	function searchUserByName($searchTerm){
		$qStr = "SELECT
					user_id, user_first_name, user_last_name, user_active
				FROM users
				WHERE user_first_name =" . $searchTerm . OR  " user_last_name =" . $searchTerm;
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_users = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user = array();
			$user['user_id'] = htmlentities($row['user_id']);
			$user['user_first_name'] = htmlentities($row['user_first_name']);
			$user['user_last_name'] = htmlentities($row['user_last_name']);
			$user['user_role'] = htmlentities($row['user_role']);

			// add the $user to the $all_users array
			$all_users[] = $user;
		}

		return $all_users;
			
	}


	/*
	* Get photoTagID and tagDescription ny file_id
	*
	* @param number 		searchTerm
	*
	* @return array 	Returns an assoc array with galleries with same description
	*/
	function searchGalleriesByDescription($searchTerm){
		$qStr = "SELECT
					galleryID, galleryDescription, user_id
				FROM tblgalleries
				WHERE galleryDescription = " . $searchTerm;
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_user_galleries = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user_galleries = array();
			$user_galleries['galleryID'] = htmlentities($row['galleryID']);
			$user_galleries['galleryDescription'] = htmlentities($row['galleryDescription']);
			$user_galleries['user_id'] = htmlentities($row['user_id']);
			
			// add the $user to the $all_users array
			$all_user_galleries[] = $user_galleries;
		}

		return $all_user_galleries;
			
	}



		/**
	* Get photoTagID and tagDescription ny file_id
	*
	* @param number 		searchTerm
	*
	* @return array 	Returns an assoc array with files with the same tag
	*/
	function searchPhotosByTag($searchTerm){

		// prevent SQL injection
		$file_id = mysqli_real_escape_string($this->link, $file_id);
		

		$qStr = "SELECT *
				FROM files
				INNER JOIN tblphototags
				ON  tblphototags.file_id = tbltags.file_id
				INNER JOIN tbltags
				ON tblphototags.tagID = tbltags.tagID
				WHERE  tblphototags.tagDescription =' . $searchTerm . 'AND  tbltags.tag_active = 'yes' ";
		// If you want a fuzzy search, change the '=' to LIKE
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		
		$photosByTag = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user_photos = array();
			$user_photos['file_id'] = htmlentities($row['file_id']);
			$user_photos['file_name'] = htmlentities($row['file_name']);
			$user_photos['file_extension'] = htmlentities($row['file_extension']);
			$user_photos['file_size'] = htmlentities($row['file_size']);
			$user_photos['file_uploaded_by_id'] = htmlentities($row['file_uploaded_by_id']);
			$user_photos['file_uploaded_date'] = htmlentities($row['file_uploaded_date']);
			$user_photos['file_deleted_by_id'] = htmlentities($row['file_deleted_by_id']);
			$user_photos['file_deleted_date'] = htmlentities($row['file_deleted_date']);
			$user_photos['photo_active'] = htmlentities($row['photo_active']);
			$user_photos['photoDescription'] = htmlentities($row['photoDescription']);
			
			// add the $photoTag to the $tagsOnPhoto array
			$photosByTag[] = $user_photos;
		}

		return $photosByTag;
			
	}


	/*	End of search functions for SearchDataAccess
		The rest of this class had functions that can be altered to become search functions

	functions

		searchUserExists 
		searchGalleryExists
		searchTagExists
		searchDateExists
			return row counts to show results


		searchPhotoByDate - use a between for a range of dates
		
	*/

	/**
	* Gets a user by the id that is passed in.
	*
	* @param number 	The id of he user to get
	*
	* @return array 	Returns an assoc array (or a User obj?)
	* 					Returns false if something goes wrong.
	*/
	function get_user_by_id($id){

		$qStr = "SELECT
					user_id, user_first_name, user_last_name, user_email, user_role, user_password, user_active, user_image
				FROM users
				WHERE user_id = " . mysqli_real_escape_string($this->link, $id);
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		if($result->num_rows == 1){

			$row = mysqli_fetch_assoc($result);

			$user = array();
			$user['user_id'] = htmlentities($row['user_id']);
			$user['user_first_name'] = htmlentities($row['user_first_name']);
			$user['user_last_name'] = htmlentities($row['user_last_name']);
			$user['user_email'] = htmlentities($row['user_email']);
			$user['user_role'] = htmlentities($row['user_role']);
			$user['user_password'] = htmlentities($row['user_password']);
			$user['user_active'] = htmlentities($row['user_active']);
			$user['user_image'] = htmlentities($row['user_image']);

			return $user;
			
		}else{
			$this->handle_error("something went wrong");
		}
	}




	/**
	* Gets all the user roles from the db
	*
	* @return array
	*/
	function get_user_roles(){
		
		$qStr = "SELECT user_role_id, user_role_name FROM user_roles";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_roles = array();

		while($row = mysqli_fetch_assoc($result)){
			$role = array();
			$role['user_role_id'] = htmlentities($row['user_role_id']);
			$role['user_role_name'] = htmlentities($row['user_role_name']);
			$all_roles[] = $role;
		}

		return $all_roles;
	}

	/**
	* Handles errors in UserDataAccess
	* 
	* @param array Returns an array of User objects??? Or an array of associative arrays???
	*/
	function handle_error($msg){
        //die("handle error  "  . $msg);

        // If the user being inserted already exists (same user_email) then the db will return this error msg:
        // 'Duplicate entry ...'
        // But how should we notify the user of the problem?????
        // Let's leave that up to the client code, we'll throw an error that they can catch (if they choose to do so)
        if(strpos($msg, "Duplicate entry") !== FALSE){ // I think 'Duplicate entry' is an error from the db
            throw new Exception(self::DUPLICATE_USER_ERROR); 
        }else{
            // how do we want to handle this? should we throw an exception
            // and let our custom EXCEPTION handler deal with it?????
            $stack_trace = print_r(debug_backtrace(), true);
            throw new Exception($msg . " - " . $stack_trace);
        }
 	}

	// NOTE: we could make a DataAccess super class that has handle_error()
	// in it. Then we could sub class it and all sub classes could share the
	// same method (less code duplication)

	function get_all_user_photos(){
		$qStr = "SELECT
					file_id, file_name, file_extension, file_size, file_uploaded_by_id, file_uploaded_date, file_deleted_by_id, file_deleted_date, photo_active, photoDescription
				FROM files
				WHERE file_uploaded_by_id = " . $_SESSION['user_id'] . "
					AND photo_active = 'yes'";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_user_photos = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user_photos = array();
			$user_photos['file_id'] = htmlentities($row['file_id']);
			$user_photos['file_name'] = htmlentities($row['file_name']);
			$user_photos['file_extension'] = htmlentities($row['file_extension']);
			$user_photos['file_size'] = htmlentities($row['file_size']);
			$user_photos['file_uploaded_by_id'] = htmlentities($row['file_uploaded_by_id']);
			$user_photos['file_uploaded_date'] = htmlentities($row['file_uploaded_date']);
			$user_photos['file_deleted_by_id'] = htmlentities($row['file_deleted_by_id']);
			$user_photos['file_deleted_date'] = htmlentities($row['file_deleted_date']);
			$user_photos['photo_active'] = htmlentities($row['photo_active']);
			$user_photos['photoDescription'] = htmlentities($row['photoDescription']);

			// add the $user to the $all_users array
			$all_user_photos[] = $user_photos;
		}

		return $all_user_photos;
			
	}

	function get_all_user_galleries(){
		$qStr = "SELECT
					galleryID, galleryDescription, user_id
				FROM tblgalleries
				WHERE user_id = " . $_SESSION['user_id'];
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_user_galleries = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user_galleries = array();
			$user_galleries['galleryID'] = htmlentities($row['galleryID']);
			$user_galleries['galleryDescription'] = htmlentities($row['galleryDescription']);
			$user_galleries['user_id'] = htmlentities($row['user_id']);
			
			// add the $user to the $all_users array
			$all_user_galleries[] = $user_galleries;
		}

		return $all_user_galleries;
			
	}

	function get_all_other_photos(){
		$qStr = "SELECT
					file_id, file_name, file_extension, file_size, file_uploaded_by_id, file_uploaded_date, file_deleted_by_id, file_deleted_date, photo_active, photoDescription
				FROM files
				WHERE file_uploaded_by_id <> " . $_SESSION['user_id'] . "
					AND photo_active = 'yes'";
		// If this query is bad, maybe the single quotes of 'yes' need to be escaped
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_other_photos = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$other_photos = array();
			$other_photos['file_id'] = htmlentities($row['file_id']);
			$other_photos['file_name'] = htmlentities($row['file_name']);
			$other_photos['file_extension'] = htmlentities($row['file_extension']);
			$other_photos['file_size'] = htmlentities($row['file_size']);
			$other_photos['file_uploaded_by_id'] = htmlentities($row['file_uploaded_by_id']);
			$other_photos['file_uploaded_date'] = htmlentities($row['file_uploaded_date']);
			$other_photos['file_deleted_by_id'] = htmlentities($row['file_deleted_by_id']);
			$other_photos['file_deleted_date'] = htmlentities($row['file_deleted_date']);
			$other_photos['photo_active'] = htmlentities($row['photo_active']);
			$other_photos['photoDescription'] = htmlentities($row['photoDescription']);

			// add the $user to the $all_users array
			$all_other_photos[] = $other_photos;
		}

		return $all_other_photos;
			
	}
	//get all photos

	function get_all_photos(){
		$qStr = "SELECT
					file_id, file_name, file_extension, file_size, file_uploaded_by_id, file_uploaded_date, file_deleted_by_id, file_deleted_date, photo_active, photoDescription
				FROM files";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_photos = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$photo = array();
			$photo['file_id'] = htmlentities($row['file_id']);
			$photo['file_name'] = htmlentities($row['file_name']);
			$photo['file_extension'] = htmlentities($row['file_extension']);
			$photo['file_size'] = htmlentities($row['file_size']);
			$photo['file_uploaded_by_id'] = htmlentities($row['file_uploaded_by_id']);
			$photo['file_uploaded_date'] = htmlentities($row['file_uploaded_date']);
			$photo['file_deleted_by_id'] = htmlentities($row['file_deleted_by_id']);
			$photo['file_deleted_date'] = htmlentities($row['file_deleted_date']);
			$photo['photo_active'] = htmlentities($row['photo_active']);
			$photo['photoDescription'] = htmlentities($row['photoDescription']);

			// add the $photo to the $all_photos array
			$all_photos[] = $photo;
		}

		return $all_photos;
			
	}






	function get_photos_by_galleryID(){
		$galleryIdforQuery = $_SESSION['selectedGallery'];

//SELECT * FROM `tblgalleryphotos` WHERE galleryID = 1
			$qStr = "SELECT
					file_id, galleryID, galleryPhotoId, photoActiveInGallery
				FROM  tblgalleryphotos
				WHERE galleryID = " . $galleryIdforQuery ."" ;

		

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_gallery_photos = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$gallery_photo = array();
			$gallery_photo['file_id'] = htmlentities($row['file_id']);
			$gallery_photo['galleryID'] = htmlentities($row['galleryID']);
			$gallery_photo['galleryPhotoId'] = htmlentities($row['galleryPhotoId']);
			$gallery_photo['photoActiveInGallery'] = htmlentities($row['photoActiveInGallery']);
			

			// add the $user to the $all_users array
			$all_gallery_photos[] = $gallery_photo;
		}

		return $all_gallery_photos;
			
	}





}