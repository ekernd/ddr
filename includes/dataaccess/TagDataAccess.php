<?php
class TagDataAccess{
	
	private $link;
	const DUPLICATE_TAG_ERROR = "That tag already exists";
	/**
	 * Constructor
	 *
	 * @param connection $link 	The link the the database 		
	 */
	function __construct($link){
		$this->link = $link;
	}
	


	/**
	* Gets all tags
	* 
	* @return array Returns an array of tags  
	* 				
	*/
	function get_all_tags(){
		$qStr = "SELECT
					*
				FROM tbltags" ;
		// If this query is bad, maybe the single quotes of 'yes' need to be escaped
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_tags = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$tag = array();
			$tag['tagID'] = htmlentities($row['tagID']);
			$tag['tagDescription'] = htmlentities($row['tagDescription']);
			$tag['tag_active'] = htmlentities($row['tag_active']);
			

			// add the $tag to the $all_tags array
			$all_tags[] = $tag;
		}

		return $all_tags;
			
	}


	/**
	* Inserts a new tag into the tags table
	*
	* @param array 		An obj/array that has the following properties: 
	*					tagDescription, tag_active
	*
	* @return array 	Returns an assoc array, along with the new users id
	* 					Returns false if something goes wrong.
	*/
	function insert_tag($tag){

		// prevent SQL injection
		$tag['tagDescription'] = mysqli_real_escape_string($this->link, $tag['tagDescription']);
		$tag['tag_active'] = mysqli_real_escape_string($this->link, $tag['tag_active']);
	

		

		$qStr = "INSERT INTO tbltags (
					tagDescription,
					tag_active
					
				) VALUES (
					'{$tag['tagDescription']}',
					'{$tag['tag_active']}'
					
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the user id that was assigned by the data base
			$tag['tagID'] = mysqli_insert_id($this->link);
			// then return the user
			return $tag;
		}else{
			$this->handle_error("unable to insert tag");
		}

		return false;
	}

	/**
	* Deactivates an existing tag in the tags table
	*
	* @param array 		An obj/array that has the following properties: 
	*					tagID,  tag_active
	*
	* @return array 	Returns an assoc array with all the tag properties
	* 					Returns false if something goes wrong.
	*/
	function update_tag($tag){

		// prevent SQL injection
		$tag['tagID'] = mysqli_real_escape_string($this->link, $tag['tagID']);
		//$tag['tagDescription'] = mysqli_real_escape_string($this->link, $tag['tag_first_name']);
		$tag['tag_active'] = mysqli_real_escape_string($this->link, $tag['tag_active']);
		

	

		$qStr = "UPDATE tbltags SET
			
				tag_active = '{$tag['tag_active']}'
				WHERE tagID = " . $tag['tagID'];
					
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $tag;
		}else{
			$this->handle_error("unable to update tag");
		}

		return false;
	}

	

	function handle_error($msg){
        //die("handle error  "  . $msg);

        // If the user being inserted already exists (same user_email) then the db will return this error msg:
        // 'Duplicate entry ...'
        // But how should we notify the user of the problem?????
        // Let's leave that up to the client code, we'll throw an error that they can catch (if they choose to do so)
        if(strpos($msg, "Duplicate entry") !== FALSE){ // I think 'Duplicate entry' is an error from the db
            throw new Exception(self::DUPLICATE_TAG_ERROR); 
        }else{
            // how do we want to handle this? should we throw an exception
            // and let our custom EXCEPTION handler deal with it?????
            $stack_trace = print_r(debug_backtrace(), true);
            throw new Exception($msg . " - " . $stack_trace);
        }
 	}
}