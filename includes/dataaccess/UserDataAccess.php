<?php
class UserDataAccess{
	
	private $link;

	const DUPLICATE_USER_ERROR = "That email is already in the db";
	
	/**
	 * Constructor
	 *
	 * @param connection $link 	The link the the database 		
	 */
	function __construct($link){
		$this->link = $link;
	}
	
	/**
	* Authenticates a user for accessing the web site (or the control panel of the site)
	* 
	* @param string email
	* @param string password
	* 
	* @return array If login is authenticated returns the user object (as assoc array or User obj???).
	* 				Returns false if authentication fails (or if something goes wrong).
	*/
	function login($email, $password){
	
		// Prevent SQL injection by scrubbing the email and password
		// with mysqli_real_escape_string();
		$email = mysqli_real_escape_string($this->link, $email);
		$password = mysqli_real_escape_string($this->link, $password);
		echo("The password entered is : " . $password);
		$qStr = "SELECT
					user_id, user_first_name, user_last_name, user_email, user_role, user_salt, user_password
				FROM users 
				WHERE user_email = '" . $email . "' AND user_active='yes'";
		
		// this comes in really handy when you are testing your query....
		//die($qStr);

		
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		if($result){

			$row = mysqli_fetch_assoc($result);
			
			$salt = $row['user_salt'];
			//die($this->encrypt_password($salt, $password));
			echo("User password from the database " . $row['user_password']);
			if($this->encrypt_password($salt, $password) == $row['user_password']){

				// scrub the data to prevent XSS attacks
				$user = array();
				$user['user_id'] = htmlentities($row['user_id']);
				$user['user_first_name'] = htmlentities($row['user_first_name']);
				$user['user_last_name'] = htmlentities($row['user_last_name']);
				$user['user_email'] = htmlentities($row['user_email']);
				$user['user_role'] = htmlentities($row['user_role']);

				return $user;
			}
		}

		return false;
	}

	/**
	* Gets all users
	* 
	* @return array Returns an array of User objects??? 
	* 				Or an array of associative arrays???
	*/
	function get_all_users(){
		$qStr = "SELECT
					user_id, user_first_name, user_last_name, user_email, user_role, user_active
				FROM users";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_users = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user = array();
			$user['user_id'] = htmlentities($row['user_id']);
			$user['user_first_name'] = htmlentities($row['user_first_name']);
			$user['user_last_name'] = htmlentities($row['user_last_name']);
			$user['user_email'] = htmlentities($row['user_email']);
			$user['user_role'] = htmlentities($row['user_role']);

			// add the $user to the $all_users array
			$all_users[] = $user;
		}

		return $all_users;
			
	}

	/**
	* Gets a user by the id that is passed in.
	*
	* @param number 	The id of he user to get
	*
	* @return array 	Returns an assoc array (or a User obj?)
	* 					Returns false if something goes wrong.
	*/
	function get_user_by_id($id){

		$qStr = "SELECT
					user_id, user_first_name, user_last_name, user_email, user_role, user_password, user_active, user_image
				FROM users
				WHERE user_id = " . mysqli_real_escape_string($this->link, $id);
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		if($result->num_rows == 1){

			$row = mysqli_fetch_assoc($result);

			$user = array();
			$user['user_id'] = htmlentities($row['user_id']);
			$user['user_first_name'] = htmlentities($row['user_first_name']);
			$user['user_last_name'] = htmlentities($row['user_last_name']);
			$user['user_email'] = htmlentities($row['user_email']);
			$user['user_role'] = htmlentities($row['user_role']);
			$user['user_password'] = htmlentities($row['user_password']);
			$user['user_active'] = htmlentities($row['user_active']);
			$user['user_image'] = htmlentities($row['user_image']);

			return $user;
			
		}else{
			$this->handle_error("something went wrong");
		}
	}

	/**
	* Inserts a new user into the Users table
	*
	* @param array 		An obj/array that has the following properties: 
	*					user_first_name, user_last_name, user_email, user_role, user_password, user_active
	*
	* @return array 	Returns an assoc array, along with the new users id
	* 					Returns false if something goes wrong.
	*/
	function insert_user($user){

		// prevent SQL injection
		$user['user_first_name'] = mysqli_real_escape_string($this->link, $user['user_first_name']);
		$user['user_last_name'] = mysqli_real_escape_string($this->link, $user['user_last_name']);
		$user['user_email'] = mysqli_real_escape_string($this->link, $user['user_email']);
		$user['user_role'] = mysqli_real_escape_string($this->link, $user['user_role']);
		//$user['user_password'] = mysqli_real_escape_string($this->link, $user['user_password']);
		$user['user_active'] = "yes";
		$user['user_image'] = "";

		//secure the password
		$salt = $this->get_password_salt();
		$password = $this->encrypt_password($salt, $user['user_password']);

		$qStr = "INSERT INTO users (
					user_first_name,
					user_last_name, 
					user_email, 
					user_password, 
					user_salt, 
					user_role, 
					user_active,
					user_image
				) VALUES (
					'{$user['user_first_name']}',
					'{$user['user_last_name']}',
					'{$user['user_email']}', 
					'{$password}', 
					'{$salt}', 
					'{$user['user_role']}', 
					'{$user['user_active']}',
					'{$user['user_image']}'
				)";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			// add the user id that was assigned by the data base
			$user['user_id'] = mysqli_insert_id($this->link);
			// then return the user
			return $user;
		}else{
			$this->handle_error("unable to insert user");
		}

		return false;
	}

	/**
	* Updates an existing user in the Users table
	*
	* @param array 		An obj/array that has the following properties: 
	*					user_id,user_first_name, user_last_name, user_email, user_role, user_password, user_active
	*
	* @return array 	Returns an assoc array with all the user properties
	* 					Returns false if something goes wrong.
	*/
	function update_user($user){

		// prevent SQL injection
		$user['user_id'] = mysqli_real_escape_string($this->link, $user['user_id']);
		$user['user_first_name'] = mysqli_real_escape_string($this->link, $user['user_first_name']);
		$user['user_last_name'] = mysqli_real_escape_string($this->link, $user['user_last_name']);
		$user['user_email'] = mysqli_real_escape_string($this->link, $user['user_email']);
		$user['user_role'] = mysqli_real_escape_string($this->link, $user['user_role']);
		//$user['user_password'] = mysqli_real_escape_string($this->link, $user['user_password']);
		$user['user_active'] = mysqli_real_escape_string($this->link, $user['user_active']);
		$user['user_image'] = mysqli_real_escape_string($this->link, $user['user_image']);

		//secure the password
		if($encrypt){
			$salt = $this->get_password_salt();
			$password = $this->encrypt_password($salt, $user['user_password']);

			$qStr = "UPDATE users SET
						user_first_name = '{$user['user_first_name']}',
						user_last_name = '{$user['user_last_name']}',
						user_email = '{$user['user_email']}', 
						user_password = '{$password}',
						user_salt = '{$salt}', 
						user_role = '{$user['user_role']}',  
						user_active = '{$user['user_active']}',
						user_image = '{$user['user_image']}'
					WHERE user_id = " . $user['user_id'];
		}else{
			$qStr = "UPDATE users SET
					user_first_name = '{$user['user_first_name']}',
					user_last_name = '{$user['user_last_name']}',
					user_email = '{$user['user_email']}', 
					user_role = '{$user['user_role']}',  
					user_active = '{$user['user_active']}',
					user_image = '{$user['user_image']}'
				WHERE user_id = " . $user['user_id'];
		}			
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		if($result){
			return $user;
		}else{
			$this->handle_error("unable to update user");
		}

		return false;
	}

	/**
	* Generates salt (a random string) for securing passords
	*
	* @return string 	returns the salt string
	*/
	function get_password_salt(){
		//$bytes = random_bytes(5);
		//return bin2hex($bytes);

		return random_bytes(5);
	}

	/**
	* Encrypts a password, using the salt provided.
	*
	* @param $salt 		The string used to salt the encrption
	* @param $password 	The password (string) to encrypt
	*/
	function encrypt_password($salt, $password){
		return md5($salt . $password . $salt);
	}

	/**
	* Gets all the user roles from the db
	*
	* @return array
	*/
	function get_user_roles(){
		
		$qStr = "SELECT user_role_id, user_role_name FROM user_roles";
		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));

		$all_roles = array();

		while($row = mysqli_fetch_assoc($result)){
			$role = array();
			$role['user_role_id'] = htmlentities($row['user_role_id']);
			$role['user_role_name'] = htmlentities($row['user_role_name']);
			$all_roles[] = $role;
		}

		return $all_roles;
	}

	/**
	* Handles errors in UserDataAccess
	* 
	* @param array Returns an array of User objects??? Or an array of associative arrays???
	*/
	function handle_error($msg){
        //die("handle error  "  . $msg);

        // If the user being inserted already exists (same user_email) then the db will return this error msg:
        // 'Duplicate entry ...'
        // But how should we notify the user of the problem?????
        // Let's leave that up to the client code, we'll throw an error that they can catch (if they choose to do so)
        if(strpos($msg, "Duplicate entry") !== FALSE){ // I think 'Duplicate entry' is an error from the db
            throw new Exception(self::DUPLICATE_USER_ERROR); 
        }else{
            // how do we want to handle this? should we throw an exception
            // and let our custom EXCEPTION handler deal with it?????
            $stack_trace = print_r(debug_backtrace(), true);
            throw new Exception($msg . " - " . $stack_trace);
        }
 	}

	// NOTE: we could make a DataAccess super class that has handle_error()
	// in it. Then we could sub class it and all sub classes could share the
	// same method (less code duplication)

	function get_all_user_photos(){
		$qStr = "SELECT
					file_id, file_name, file_extension, file_size, file_uploaded_by_id, file_uploaded_date, file_deleted_by_id, file_deleted_date, photo_active, photoDescription
				FROM files
				WHERE file_uploaded_by_id = " . $_SESSION['user_id'] . "
					AND photo_active = 'yes'";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_user_photos = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user_photos = array();
			$user_photos['file_id'] = htmlentities($row['file_id']);
			$user_photos['file_name'] = htmlentities($row['file_name']);
			$user_photos['file_extension'] = htmlentities($row['file_extension']);
			$user_photos['file_size'] = htmlentities($row['file_size']);
			$user_photos['file_uploaded_by_id'] = htmlentities($row['file_uploaded_by_id']);
			$user_photos['file_uploaded_date'] = htmlentities($row['file_uploaded_date']);
			$user_photos['file_deleted_by_id'] = htmlentities($row['file_deleted_by_id']);
			$user_photos['file_deleted_date'] = htmlentities($row['file_deleted_date']);
			$user_photos['photo_active'] = htmlentities($row['photo_active']);
			$user_photos['photoDescription'] = htmlentities($row['photoDescription']);

			// add the $user to the $all_users array
			$all_user_photos[] = $user_photos;
		}

		return $all_user_photos;
			
	}

	function get_all_user_galleries(){
		$qStr = "SELECT
					galleryID, galleryDescription, user_id
				FROM tblgalleries
				WHERE user_id = " . $_SESSION['user_id'];
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_user_galleries = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$user_galleries = array();
			$user_galleries['galleryID'] = htmlentities($row['galleryID']);
			$user_galleries['galleryDescription'] = htmlentities($row['galleryDescription']);
			$user_galleries['user_id'] = htmlentities($row['user_id']);
			
			// add the $user to the $all_users array
			$all_user_galleries[] = $user_galleries;
		}

		return $all_user_galleries;
			
	}

	function get_all_other_photos(){
		$qStr = "SELECT
					file_id, file_name, file_extension, file_size, file_uploaded_by_id, file_uploaded_date, file_deleted_by_id, file_deleted_date, photo_active, photoDescription
				FROM files
				WHERE file_uploaded_by_id <> " . $_SESSION['user_id'] . "
					AND photo_active = 'yes'";
		// If this query is bad, maybe the single quotes of 'yes' need to be escaped
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_other_photos = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$other_photos = array();
			$other_photos['file_id'] = htmlentities($row['file_id']);
			$other_photos['file_name'] = htmlentities($row['file_name']);
			$other_photos['file_extension'] = htmlentities($row['file_extension']);
			$other_photos['file_size'] = htmlentities($row['file_size']);
			$other_photos['file_uploaded_by_id'] = htmlentities($row['file_uploaded_by_id']);
			$other_photos['file_uploaded_date'] = htmlentities($row['file_uploaded_date']);
			$other_photos['file_deleted_by_id'] = htmlentities($row['file_deleted_by_id']);
			$other_photos['file_deleted_date'] = htmlentities($row['file_deleted_date']);
			$other_photos['photo_active'] = htmlentities($row['photo_active']);
			$other_photos['photoDescription'] = htmlentities($row['photoDescription']);

			// add the $user to the $all_users array
			$all_other_photos[] = $other_photos;
		}

		return $all_other_photos;
			
	}
	//get all photos

	function get_all_photos(){
		$qStr = "SELECT
					file_id, file_name, file_extension, file_size, file_uploaded_by_id, file_uploaded_date, file_deleted_by_id, file_deleted_date, photo_active, photoDescription
				FROM files";
		
		//die($qStr);

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_photos = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$photo = array();
			$photo['file_id'] = htmlentities($row['file_id']);
			$photo['file_name'] = htmlentities($row['file_name']);
			$photo['file_extension'] = htmlentities($row['file_extension']);
			$photo['file_size'] = htmlentities($row['file_size']);
			$photo['file_uploaded_by_id'] = htmlentities($row['file_uploaded_by_id']);
			$photo['file_uploaded_date'] = htmlentities($row['file_uploaded_date']);
			$photo['file_deleted_by_id'] = htmlentities($row['file_deleted_by_id']);
			$photo['file_deleted_date'] = htmlentities($row['file_deleted_date']);
			$photo['photo_active'] = htmlentities($row['photo_active']);
			$photo['photoDescription'] = htmlentities($row['photoDescription']);

			// add the $photo to the $all_photos array
			$all_photos[] = $photo;
		}

		return $all_photos;
			
	}






	function get_photos_by_galleryID(){
		$galleryIdforQuery = $_SESSION['selectedGallery'];

//SELECT * FROM `tblgalleryphotos` WHERE galleryID = 1
			$qStr = "SELECT
					file_id, galleryID, galleryPhotoId, photoActiveInGallery
				FROM  tblgalleryphotos
				WHERE galleryID = " . $galleryIdforQuery ."" ;

		

		$result = mysqli_query($this->link, $qStr) or $this->handle_error(mysqli_error($this->link));
		
		$all_gallery_photos = array();

		while($row = mysqli_fetch_assoc($result)){

			// create a $user obj and scrub the data to prevent XSS attacks
			$gallery_photo = array();
			$gallery_photo['file_id'] = htmlentities($row['file_id']);
			$gallery_photo['galleryID'] = htmlentities($row['galleryID']);
			$gallery_photo['galleryPhotoId'] = htmlentities($row['galleryPhotoId']);
			$gallery_photo['photoActiveInGallery'] = htmlentities($row['photoActiveInGallery']);
			

			// add the $user to the $all_users array
			$all_gallery_photos[] = $gallery_photo;
		}

		return $all_gallery_photos;
			
	}

}