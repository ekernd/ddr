<?php
$page_title= 'DDR User Homepage';
include("includes/config.inc.php");
include("includes/dataaccess/UserDataAccess.php");
include("includes/dataaccess/FileDataAccess.php");
include("includes/login_check.inc.php");
require_once("final-php/header_v2.php");

//var_dump(file_exists("includes/dataaccess/UserDataAccess.php"));
$included_files = get_included_files();

$link = get_link();
$user_da = new UserDataAccess($link);
//$file_da = new FileDataAccess($link);
$galleries = $user_da->get_all_user_galleries();


// echo(" <br>These are the include files:<br>");
// foreach ($included_files as $filename) {
//     echo( "$filename\n <br>");
// }
// this line sets the user name
$user_first_name =  $_SESSION['user_first_name'];
?>
<!-- added a greeting fot the user -->
<h1>WELCOME <?php echo(strtoupper($user_first_name))?> </h1>
<br>
<h3>
	<?php
		$_SESSION['selectedGallery'] = "";



	//	echo("<a href='user-details.php'>Add New User</a><br>");
		// here's how you could use the data to create a table
		echo("<table border=\"1\">");
		echo("<th>Galleries</th>");
		foreach($galleries as $gallery){
			echo("<tr>");
			
			echo("<td>" . $gallery['galleryDescription'] . "</td>");

			echo("<td><a class='button-uploadPhoto' href=\"gallery.php?galleryID=" .  $gallery['galleryID'] . "\">View gallery</a></td>");
			echo("<td><a class='button-delete' href=\"galleryMutation.php?galleryID=" .  $gallery['galleryID'] . "\">Delete gallery</a></td>");
			
			echo("</tr>");
		}
		echo("</table>");

?>
<br>
<br>
<?php 
	$file_da = new FileDataAccess($link);
	$user_da = new UserDataAccess($link);

	$photoIdandExtention = array();
	$userPhoto = array();
	$thePhotoIdsByUser = $user_da -> get_all_user_photos();
	$all_photos=$user_da->get_all_photos();
	 //var_dump($thePhotoIdsByUser);


	foreach ($thePhotoIdsByUser as $photoID ) {
		$_SESSION['file_id']= $photoID['file_id'];
		$all_photos=$user_da->get_all_photos();
			foreach ($all_photos as $p) {
			
			
				if($p['file_id'] == $photoID['file_id']){

						array_push($userPhoto, $p);
				}
			}

	}

	// var_dump($userPhoto);

	$imgDescriptions = array();

	foreach ($thePhotoIdsByUser as $photoID ) {
		// get_photoDescription_byFileID()
		$_SESSION['file_id']= $photoID['file_id'];
		$description =  $file_da->get_photoDescription_byFileID();
		//echo($description);
		
			array_push($imgDescriptions, $description);

	}



 ?>
<script type="text/javascript" src="final-js/image-gallery.js"></script>

<script>
			
		
		
		window.addEventListener("load", function() {
			
			var images = [<?php echo '"'.implode('","', $photoIdandExtention).'"' ?>];
			console.log(images);
			var imageAlts = [<?php echo '"'.implode('","', $imgDescriptions).'"' ?>];
			console.log(imageAlts);
		
			makeGallery({
	
			images:images,
			imageAlts:imageAlts
		});
	});

		</script>

		<br>

	<div id="centerTheDiv">
		<h1 ><?php echo($user_first_name); ?>'s Images</h1>
		<br>

		<div id="main-image">
		</div>
		<?php
		$selectedPhoto = 0;
		//echo("<a class=\"button-login\" href=\"photo-details.php?fileID=" .  $_SESSION['file_id'] . "\">Edit photo</a>");

		 ?>
		<!-- <a class="button-login" href="photo-details.php">Edit photo</a> -->
		
		<div id="image-gallery">
		<?php 
			 $rowCount = 0;
			 foreach ($userPhoto as $gp) {
				$key = $gp['file_id'] .'.'. $gp['file_extension'];
				// var_dump($key);
				$altTag = $gp['photoDescription'];
			//	 var_dump($altTag);
				$rowCount++;
				echo('<img src="uploaded-files/thumbnails/' . $key. '" alt = "'.$altTag. '" id = "'.$key.'" >');

					if ($rowCount >=5) {
						echo('<br>');
						$rowCount = 0;
					}
			  }
			 
		?>
			
		</div>


			</h3>
			<a class="button-login" href="user-list.php">View Users</a>
			<br>

					
	</div>



<?php
	require_once("final-php/footer_v2.php");
?>