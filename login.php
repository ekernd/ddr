<?php
$page_title= 'DDR Login';
include("includes/config.inc.php");
include("includes/dataaccess/UserDataAccess.php");

//the login page does not include the  regular header
// require_once("final-php/header.php");
require_once("final-php/headerForLoginPage.php");




if($_SERVER['REQUEST_METHOD'] == "POST"){

	$username = $_POST['txtUser'];
	$password = $_POST['txtPassword'];

	$user_da = new UserDataAccess(get_link());
	$user = $user_da->login($username, $password);
	//die(var_dump($user));

	if($user){
		//session_regenerate_id();
		$_SESSION['user_first_name'] = $user['user_first_name'];
		$_SESSION['role'] = $user['user_role'];
		$_SESSION['user_id']=$user['user_id'];

		//var_dump($_SESSION);
		//die();
		header("Location: index.php");
	}else{
		echo("Invalid user name or password");
	}

}else{
	// whenever the user comes to this page, we should destroy any session vars
	// and create a new session id (this is the 'phpsessid' cookie)
	session_destroy();
	//session_regenerate_id();  this line was causing an error
}
?>
<main>
	<div class="content-frame">
			<form class="login" method="POST" action="login.php">
				
				Username: <input type="text" name="txtUser" />
				<br>
				<br>
				Password: <input type="password" name="txtPassword" />
				<br>
				<br>
				<input type="submit" class="button-login" value="Log In" />
				<br>
				<br>
			</form>
	</div>	
</main>			
<aside>
	<div class="content-frame">
	
				New User?:<input type="button-createNewUser" class="button-login" onClick="location.href='user-details.php'" value="Create New User">
			
	</div>		
	</aside>		
<?php	
require_once("final-php/footer_v2.php");
?>

