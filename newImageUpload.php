<?php
include("includes/ImageUploader.php");
include("includes/config.inc.php");
require_once("includes/dataaccess/UserDataAccess.php");
require_once("includes/dataaccess/FileDataAccess.php");
$page_title= 'DDR New Image Upload';
require_once("final-php/header_v2.php");
$link = get_link();
$user_da = new UserDataAccess($link);


//var_dump($_FILES['picture_upload']);
        

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    // STEP 1 - Setting up the image uploader...
    // params used for the ImageUploader constructor
    $max_img_size_allowed = 4000000;                        // in bytes
    $allowed_file_types = array('gif','jpg','jpeg','png');  // we'll allow these file types to be uploaded
    
    $imgUploader = new ImageUploader($max_img_size_allowed, $allowed_file_types);



 

    $file_da = new FileDataAccess($link);
    $file = array(
    'file_name' => $_FILES['imgFile']['name'],
    'file_extension' => $file_da->get_file_extension($_FILES['imgFile']['name']),
    'file_size' =>$_FILES['imgFile']['size'],
    'file_uploaded_by_id' => $_SESSION['user_id'], // NOTE WE'LL CHANGE THIS LATER TODO session variable user ID
    'file_uploaded_date' => date("Y-m-d"),
    'photoDescription' => $_POST['photoDescription']
    
    // 'photoDescription' =>
    );

    if ($file = $file_da->insert_file($file)) {
                   // STEP 2 - Uploading an image...
        $upload_dir = "uploaded-files/";    // where to put the file once it has been uploaded
        $file_posted = $_FILES['imgFile'];  // the file input from the HTML form
           // $new_file_name = time();            // You probably need to rename the file (using the id from the database),
        $new_file_name = $file['file_id'];                             // but since we aren't using a db for example, we'll just use a time stamp for the id
                                        // Note that the ImageUploader will add the proper extension to the new file name;
        //var_dump($_FILES);
       // var_dump("File_name   ----  " . $_FILES['imgFile']['name']);
            // now that we've inserted a row for the file in the files table
            // lets upload the file...
        try {
           // var_dump("The new file name is supposed to be " . $new_file_name);
            //  upload the image...
            $upload_result = $imgUploader->uploadImage($new_file_name, $file_posted, $upload_dir);
           // var_dump("the file in db " . $file['file_id']);
            // $upload_result will be an array that holds information about the file
            // but it something goes wrong it will be FALSE
            if ($upload_result ==  false) {
                die("Something when wrong uploading the image.");
            }
        } catch (Exception $e) {
            $error_messages['imgFile'] = $e->getMessage();
        }
            // Now that the image has been uploaded, let's rename it so that it
            // is named by the file id, rather than the original name
            $new_file_name = $file['file_id'] . "." . $file['file_extension'];
         //   var_dump("the new file name is   " . $upload_dir. $new_file_name);
            
         //   var_dump("the new file name is   " . $new_file_name);

        // if (rename($upload_dir . $file['file_name'], $upload_dir . $new_file_name) === false) {
        //     $error_messages['imgFile'] = "Unable to rename file after uploading";
        // }

            // update the $user array so that it inserts/updates the new file name in the users table
            $user['user_image'] = $new_file_name;

    
        //var_dump($upload_result);die();
    } else {
        $error_messages['imgFile'] = "Unable to insert file into db.";
    }


// STEP 3 - resizing the image
// we'll put the resized images in a 'thumbnails' folder that is inside of the upload_dir
    $thumbnails_dir = $upload_dir . "thumbnails/";
    
    $img_src = $upload_result['file_path'];                                     // the path to the image being resized
    $img_destination =  $thumbnails_dir .  $upload_result['file_name'];         // the destination folder for the resized image
    $resize_width = 150;
    $resize_height = 200;

    


// resize the image...
    $resize_result = $imgUploader->resizeImage($img_src, $img_destination, $resize_width, $resize_height);

// $resize_result will be an array that has some details about the final size of the image
    if ($resize_result ==  false) {
        die("Something when wrong resizing the image.");
    }
//var_dump($resize_result);die();
$theNewFile = $thumbnails_dir . $file["file_id"]. "." . $file["file_extension"];
// var_dump($theNewFile);

// echo "<img src='$theNewFile'>";
//     die("Looks like it worked! Check the uploaded-files folder!");


    
    //go to photo details page
     header('Location: photo-details.php');
}

$discMessage = "Please add a description to the photo prior to upload."

?>

   
<div class="imgUpPage">
    

    <form method="POST" action="<?php echo($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
        <textarea rows="" cols="40" value="" name = "photoDescription"><?php echo $discMessage; ?></textarea>
        <br><br>
        
        <input class="button" type="file" name="imgFile">
        <input class="button-uploadPhoto" type="submit"  name="Submit" value="Upload">
        
    </form>
    



</div>
<?php
    require_once("final-php/footer_v2.php");
?>

</html>
