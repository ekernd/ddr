<?php
if(!session_id()) session_start();
$theNewFile = $_SESSION['theNewFile'];
// include("includes/config.inc.php");
if(isset($_GET['param']) && !empty($_GET['param'])){
$myVar = $_GET["param"];

	$theNewFile = $myVar;
	$_SESSION['theNewFile']=$myVar;
}
$page_title= 'DDR Edit Photo Details';
require_once("includes/dataaccess/TagDataAccess.php");
require_once("includes/dataaccess/UserDataAccess.php");
require_once("includes/dataaccess/PhotoTagsDataAccess.php");
require_once("includes/dataaccess/FileDataAccess.php");
include("includes/ImageUploader.php");
include("includes/config.inc.php");

require_once("final-php/header_v2.php");


$link = get_link();
$tag_da = new TagDataAccess($link);
$user_da = new UserDataAccess($link);
$photTags_da = new PhotoTagsDataAccess($link);
$file_da = new FileDataAccess($link);


if(isset($_GET['tagID']) && !empty($_GET['tagID'])){
$tagToRemoveFromPhoto = $_GET["tagID"];
$deleteResult = $photTags_da->delete_tag($tagToRemoveFromPhoto);
	if ($deleteResult) {
		header('Location: photo-details.php');
	}
}




$thePhotoDescription;
// 			$tag = array();
// 			$tag['tagID'] = htmlentities($row['tagID']);
// 			$tag['tagDescription'] = htmlentities($row['tagDescription']);
// 			$tag['tag_active'] = htmlentities($row['tag_active']);

$all_tags=$tag_da->get_all_tags();

// var_dump($all_tags);
// die();
// $tagDescritions =[];

// foreach($all_tags as $tag)
// {
// 	// var_dump($tag);

// 	 foreach ($tag as $t) {
// 		//echo($t['tagDescription']);
// 		array_push($tagDescritions, $t['tagDescription']);
// 	 }
   
// }
//var_dump($tagDescritions);
// set up an empty user obj/array
$all_photo_tags=$photTags_da->get_all_photo_tags();




$all_photos=$user_da->get_all_photos();
// var_dump($all_photos);//

//get the file ID number from the file name
$fileID = substr($theNewFile,0,strpos($theNewFile,'.')) ;
//echo($fileID);
// var_dump($fileID);
foreach ($all_photos as $photo) {
	if($photo['file_id']==$fileID){
		
		$thePhotoDescription = $photo['photoDescription'];
		 $photo['photoDescription'];
	}
}
//get all tags on this photo
$tags_ids_current_photo= array();
foreach ($all_photo_tags as $key ) {
	if ($fileID==$key['file_id']) {
		array_push($tags_ids_current_photo, $key['tagID']);
	}
}
//var_dump($tags_ids_current_photo);


$textualTagsOnPhoto =array();

foreach ($all_tags as $keyOuter ) {
	foreach ($tags_ids_current_photo as $keyInner) {
		if ($keyOuter['tagID'] == $keyInner) {
			//filter out duplicate tags
			if (in_array($keyOuter['tagDescription'],$textualTagsOnPhoto)==false) {
				array_push($textualTagsOnPhoto, $keyOuter);
			}
			
		}
	}
	
}
//var_dump($all_photo_tags);

$photoTagsByFileID = $photTags_da->getPhotoTagsByFileID($fileID);
//var_dump($photoTagsByFileID);
// die();


$tag = array();
$tag['tagID'] = 0; // set to 0, this will help us determine if we need to update or insert later
$tag['tagDescription'] = "";
$tag['tag_active'] = "";

$newTagInvolvedInPost = true;
$newTaginDB=array();
// check to see if the form is being posted (there are other ways to do this)
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	//var_dump($_POST);
	 
	if ($_POST['tagID']=='Or Choose Existing Tag'&&$_POST['txtNewTag']==''){
		$newTagInvolvedInPost = false;
	}

	// The form is being posted, so we must be either
	// creating a new user, or editing an existing one

	// get all the values entered by the user
	if (!$_POST['txtNewTag']=='') {
		
		
		$tag['tagDescription']=$_POST['txtNewTag'];
		$tag['tag_active'] ='yes';
	



	// $error_messages = validate_input($tag);
	// if(empty($error_messages)){
	if(true){
		try{
	
			$newTaginDB=$tag_da->insert_tag($tag);

			
		}catch(Exception $e){
			// die("FOO: ". $e ->getMessage());
			if($e->getMessage() == TagDataAccess::DUPLICATE_TAG_ERROR){
					$error_messages['tagDescription'] = "The tag already exists";
			}
		}
	}
}

// var_dump($tag);
// 	die();
	if ($_POST['tagID']>0||count($newTaginDB)>0) {

					if(count($newTaginDB)>0){
					$tagPhoto['tagID'] =$newTaginDB['tagID'];
					$tagPhoto['file_id']=$_POST['txtFileID'];

					}else{

					$tagPhoto['tagID'] =$_POST['tagID'];
					$tagPhoto['file_id']=$_POST['txtFileID'];

					}


			
			try{
		
				$newTagPhoto=	$photTags_da->insert_photoTag($tagPhoto);
				// var_dump($tagPhoto);
				 //($newTagPhoto);
			
			}catch(Exception $e){
				// die("FOO: ". $e ->getMessage());
				if($e->getMessage() == TagDataAccess::DUPLICATE_TAG_ERROR){
						$error_messages['tagDescription'] = "The tag already exists";
				}
			}
		}

	

	if ($newTagInvolvedInPost == false&&($photo['photoDescription']!=$_POST['txtPhotoDescription'])&&trim($_POST['txtPhotoDescription'])!='') {
				$file = array();
				$file["file_id"]=$_POST['txtFileID'];
				$file['photoDescription']=$_POST['txtPhotoDescription'];
			{
			try{
		
					$file_da->update_file_description($file);
				// var_dump($file);
				// var_dump($newTagPhoto);
			
			}catch(Exception $e){
				// die("FOO: ". $e ->getMessage());
				if($e->getMessage() == TagDataAccess::DUPLICATE_TAG_ERROR){
						$error_messages['tagDescription'] = "The tag already exists";
				
			}
		}
	}
	}
	header('Location: photo-details.php');

}




function validate_input($tag){
	
	// we'll popluate an array with all the error message
	// Note that if the form is valid, then this array will be empty
    // and we'll check this to see if we should send the data to the db
    // $tag['tagDescription'] = $_POST['tagDescription'];
    // 	$tag['tag_active'] = $_POST['tag_active'];  
	$error_messages = array();

	// tag
    // $tag['tagDescription'] = $_POST['tagDescription'];
	if(empty($tag['tagDescription'])){
		$error_messages['tagDescription'] = "Tag description cannot be empty";
	}

	

	//active
	if( ($tag['tag_active'] == "yes" || $tag['tag_active'] == "no") == FALSE){
		$error_message['tag_active'] = "Active is not valid - I suspect FOWL PLAY!";
		// send email to site admin???
	}

	return $error_messages;
}

	
?>
	<div id="indexBackgroundR">

		<main>

			<div class="content-frame">
				<?php 
					
						echo('<img id="myImg" largesource= uploaded-files/' . $theNewFile. ' src=uploaded-files/thumbnails/' . $theNewFile. ' alt="'. $thePhotoDescription. '">');
					?>
				<script type="text/javascript" src="final-js/photo-details.js"></script>


				<h3>View and Edit Picture Details</h3>

				<!-- The Modal -->
				<div id="myModal" class="modal">
				<span class="close">&times;</span>
				<img class="modal-content" id="img01">
				<div id="caption"></div>
				</div>

				<br>
				<?php 
				$counter=0;
						echo "<table id='theTagsTable'>";
						echo 'Current photo tags:';
						echo "<tr>";
								foreach($photoTagsByFileID as $pt){
									// var_dump($pt);
									$counter++;
									

										
								echo "<td class='currentTag' id ={$pt['photoTagsID']}>{$pt['tagDescription']}<img src='mActionDeleteSelected.png' class='delete' id ={$pt['photoTagsID']}></td>";
												
									if ($counter > 5) {
										echo "</tr>";
										$counter = 0;
										echo "<tr>";
									}
									
								}
						echo "</table>";
							
				?>

				<b class="photoDescr">Current photo description:</b>


				<form id="photoEdit" method="POST" action="<?php echo($_SERVER['PHP_SELF'])?>" enctype="multipart/form-data">

					<textarea rows="" cols="40" value="" name="txtPhotoDescription" placeholder="<?php echo ($thePhotoDescription) ?>"></textarea>
					<br>
					<br>
					<input type="hidden" name="txtFileID" id="txtFileID" value="<?php echo($fileID); ?>" />
					<br>
					<!-- <input type="hidden" name="txttagID" id="txttagID" value="<?php echo($tag['tagID']); ?>" /><br> -->

					Create New Tag:
					<input type="text" name="txtNewTag" id="txtNewTag" value="<?php echo($tag['tagDescription']); ?>" />
					<?php echo(isset($error_messages['tagDescription']) ? $error_messages['tagDescription'] : "" ); ?>


					<br>
					<br>
					<br>

					<select name="tagID">
						<option selected="selectedTagDescription"> Or Choose Existing Tag</option>
					<?php
				
					foreach($all_tags as $item){
					?>


							<option value=<?php echo $item[ 'tagID']; ?> >
								<?php echo $item['tagDescription']; ?>
							</option>
							<?php
							}
							?>

					</select>

					<?php echo(isset($error_messages['tagDescription']) ? $error_messages['tagDescription'] : "" ); ?>
					<br>



					<!-- <a class="button-search" href="#">Submit</a> -->
					<input type="submit" class="button-search" name="btnSubmit" value="submit" />
				</form>

			</div>
		</main>

<style>
#mainPhoto {
	float: left;
	width: 60%;
}

#thePhotoTags {
	position: absolute;
	top: 0;
	bottom: 0;
	right: 0px;
	float: left;
	width: 40%;
}

table,
th,
td {
	border: none;
	min-height: 100px;
	border-collapse: collapse;
}

.delete {
	height: 50px;
	display: none;
	transition-delay: 0.5s;
}

.currentTag:hover .delete {
	display: table;

}

tr {
	min-height: 100px;
}

td:hover {
	background-color: black;
	font-size: 20px;
	color: white;
	border-radius: 25px;
}

#textarea {
	padding: 2px;
}

#theTagsTable {
	min-height: 100px;
}

#myImg {
border-radius: 5px;
cursor: pointer;
transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
}

/* Caption of Modal Image */
#caption {
	margin: auto;
	display: block;
	width: 80%;
	max-width: 700px;
	text-align: center;
	color: #ccc;
	padding: 10px 0;
	height: 150px;
}

/* Add Animation */
.modal-content, #caption {    
	-webkit-animation-name: zoom;
	-webkit-animation-duration: 0.6s;
	animation-name: zoom;
	animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
	from {-webkit-transform:scale(0)} 
	to {-webkit-transform:scale(1)}
}

@keyframes zoom {
	from {transform:scale(0)} 
	to {transform:scale(1)}
}

/* The Close Button */
.close {
	position: absolute;
	top: 15px;
	right: 35px;
	color: #f1f1f1;
	font-size: 40px;
	font-weight: bold;
	transition: 0.3s;
}

.close:hover,
.close:focus {
	color: #bbb;
	text-decoration: none;
	cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
	.modal-content {
		width: 100%;
	}
}


</style>
	</div>
	<?php
	require_once("final-php/footer_v2.php");
	?>