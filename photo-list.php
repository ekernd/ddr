<?php
include("includes/config.inc.php");
require_once("includes/dataaccess/UserDataAccess.inc.php");

?>

<!DOCTYPE html>
	<html>
	<head>
		<title>Photo List</title>
	</head>
	<body>
		<h1>YOUR PHOTOS  <span id="search-result">Photo source: all, gallery name, etc.</span></h1><br>
		<!-- The id of the span is search-result.  The name of the gallery or "All Photos" goes there. -->
		<div id=photo-thumbs>Thumbnails go here</div>
		<br>
		<form action="" method="GET">
			<input type="text" name="Search">
<!-- 			<?php
				$query = "SELECT * from files";	
				$result = mysql_query($query);

				echo($result);		
			?> -->
<!-- <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "focus_ddr";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT file_name FROM files";
$result = $conn->query($sql);

echo json_encode($result);


if ($result->num_rows > 0) {
    echo "<table><tr><th>ID</th><th>Name</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>".["file_name"]." </td></tr>";
        echo print_r($result, true);
    }
    echo "</table>";
	} else {
    echo "0 results";
}
$conn->close();
?>  -->

<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "focus_ddr";

$conn = mysqli_connect($servername, $username, $password, $dbname);


if (!$conn) {
    echo "Unable to connect to DB: " . mysql_error();
    exit;
}

if (!mysql_select_db($dbname)) {
    echo "Unable to select mydbname: " . mysql_error();
    exit;
}

$sql = "SELECT file_name FROM files";

$result = mysql_query($sql);

if (!$result) {
    echo "Could not successfully run query ($sql) from DB: " . mysql_error();
    exit;
}

if (mysql_num_rows($result) == 0) {
    echo "No rows found, nothing to print so am exiting";
    exit;
}

// While a row of data exists, put that row in $row as an associative array
// Note: If you're expecting just one row, no need to use a loop
// Note: If you put extract($row); inside the following loop, you'll
//       then create $userid, $fullname, and $userstatus
while ($row = mysql_fetch_assoc($result)) {
    echo $row["file_id"];
    echo $row["file_name"];
    echo $row["file_extension"];
    echo $row["file_size"];
    echo $row["file_uploaded_by_id"];
    echo $row["file_uploaded_date"];
    echo $row["file_deleted_by_id"];
    echo $row["file_deleted_date"];
    echo $row["photo_active"];
    echo $row["photoDescription"];
}

mysql_free_result($result);

?>
			<input type="submit" value="Search Photos">

		</form>
		<button>Add Photo</button>
		<button>Edit Photo</button>
		<button>Delete Photo</button>
	</body>
</html>