-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2017 at 04:28 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `focus_ddr`
--

-- --------------------------------------------------------

--
-- Table structure for table `files`
--
DROP TABLE IF EXISTS files;
CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `file_extension` varchar(10) NOT NULL,
  `file_size` int(11) NOT NULL,
  `file_uploaded_by_id` int(11) NOT NULL,
  `file_uploaded_date` date NOT NULL,
  `file_deleted_by_id` int(11) NOT NULL,
  `file_deleted_date` date NOT NULL,
  `photo_active` enum('yes','no') NOT NULL,
  `photoDescription` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--



-- --------------------------------------------------------

--
-- Table structure for table `tblgalleries`
--
DROP TABLE IF EXISTS tblgalleries;
CREATE TABLE `tblgalleries` (
  `galleryID` int(11) NOT NULL,
  `galleryDescription` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblgalleries`
--

INSERT INTO `tblgalleries` (`galleryID`, `galleryDescription`, `user_id`) VALUES
(1, 'Gallery 1', 9),
(2, 'Gallery 2', 9),
(3, 'Gallery 2', 1),
(4, 'Gallery 4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblgalleryphotos`
--
DROP TABLE IF EXISTS tblgalleryphotos;
CREATE TABLE `tblgalleryphotos` (
  `galleryPhotoId` int(11) NOT NULL,
  `galleryID` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `photoActiveInGallery` enum('yes','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblgalleryphotos`
--

INSERT INTO `tblgalleryphotos` (`galleryPhotoId`, `galleryID`, `file_id`, `photoActiveInGallery`) VALUES
(1, 1, 1, 'yes'),
(2, 1, 2, 'yes'),
(3, 1, 3, 'yes'),
(4, 1, 4, 'yes'),
(6, 2, 5, 'yes'),
(7, 3, 1, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `tblphototags`
--
DROP TABLE IF EXISTS tblphototags;
CREATE TABLE `tblphototags` (
  `photoTagsID` int(11) NOT NULL,
  `tagID` int(11) NOT NULL,
  `file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblphototags`
--

INSERT INTO `tblphototags` (`photoTagsID`, `tagID`, `file_id`) VALUES
(1, 1, 1),
(2, 5, 4),
(3, 4, 3),
(4, 3, 2),
(5, 5, 7),
(6, 2, 7),
(7, 7, 13),
(8, 7, 13),
(9, 6, 13),
(10, 5, 13),
(11, 7, 1),
(12, 5, 4),
(13, 6, 4),
(14, 16, 4),
(15, 2, 1),
(16, 3, 4),
(17, 20, 0),
(18, 21, 0),
(19, 22, 4),
(20, 6, 4),
(21, 22, 4),
(22, 2, 4),
(23, 23, 4),
(24, 24, 4),
(25, 7, 4),
(26, 1, 4),
(27, 24, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbltags`
--
DROP TABLE IF EXISTS tbltags;
CREATE TABLE `tbltags` (
  `tagID` int(11) NOT NULL,
  `tagDescription` varchar(50) NOT NULL,
  `tag_active` enum('yes','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltags`
--

INSERT INTO `tbltags` (`tagID`, `tagDescription`, `tag_active`) VALUES
(1, 'bird', 'yes'),
(2, 'dog', 'yes'),
(3, 'tree', 'yes'),
(4, 'sun set', 'yes'),
(5, 'blue', 'yes'),
(6, 'cat', 'yes'),
(7, 'Grapes', 'yes'),
(8, 'happy birthday', ''),
(10, 'choir concert', 'yes'),
(11, 'new baby', 'yes'),
(12, 'first day of school', 'yes'),
(13, 'graduation', 'yes'),
(14, 'wedding', 'yes'),
(16, 'vacation', 'yes'),
(17, 'blob', 'yes'),
(18, 'apple', 'yes'),
(19, 'one', 'yes'),
(20, 'odd mole', 'yes'),
(21, 'fungus', 'yes'),
(22, 'work of art', 'yes'),
(23, 'yummy sushi', 'yes'),
(24, 'favourite dress', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `thumbnails`
--
DROP TABLE IF EXISTS thumbnails;
CREATE TABLE `thumbnails` (
  `thumbnailID` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `file_extension` varchar(10) NOT NULL,
  `file_size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
DROP TABLE IF EXISTS users;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_first_name` varchar(30) NOT NULL,
  `user_last_name` varchar(30) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` char(32) NOT NULL,
  `user_salt` char(32) NOT NULL,
  `user_role` int(11) NOT NULL DEFAULT '1',
  `user_active` enum('yes','no') NOT NULL DEFAULT 'yes',
  `user_image` varchar(30) DEFAULT NULL
  
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_first_name`, `user_last_name`, `user_email`, `user_password`, `user_salt`, `user_role`, `user_active`, `user_image`) VALUES
(1, 'John', 'Doe', 'john@doe.com', 'opensesame', 'xxx', 1, 'yes', NULL),
(2, 'Jane', 'Doe', 'jane@doe.com', 'letmein', 'xxx', 2, 'yes', NULL),
(3, 'Bob', 'Smith', 'bob@smith.com', 'test', 'xxx', 2, 'yes', NULL),
(4, 'dsfgd', 'sdaf', 'asdf', 'afba40b743d6c7bc420b424c63c6e440', '?5«&b', 1, 'yes', NULL),
(5, 'dsfgdzxcv', 'sdafzxcv', 'asdfzxcv', 'df4ab5adb4c54b80453400d7b037debb', 'žÇ5-Í', 1, 'yes', NULL),
(6, 'dave', 'dave', 'ekernd@gmail.com', '7af72f3447153cb2c2fb5c13da8c3ad3', 'Žjõ%', 1, 'yes', ''),
(7, 'JIm', 'Jones', 'grape@koolaid.com', 'be4022e7db81fee43dfa888fbb0be2d3', '­çoï', 1, 'yes', ''),
(8, 'David', 'Ekern', 'ekernd2@students.westerntc.edu', 'ae743033cd0bc805ce9f2da64afc2cdc', '1¢þ7', 1, 'yes', ''),
(9, 'test', 'test', 'test@TEST.COM', '29e5fdfad0ea5cfd08965737d8501e21', 'áÇ]<Â', 1, 'yes', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--
DROP TABLE IF EXISTS user_roles;
CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL,
  `user_role_name` varchar(30) NOT NULL,
  `user_role_desc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `user_role_name`, `user_role_desc`) VALUES
(1, 'Standard User', 'Normal user with no special permissions'),
(2, 'Admin', 'Extra permissions');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `tblgalleries`
--
ALTER TABLE `tblgalleries`
  ADD PRIMARY KEY (`galleryID`);

--
-- Indexes for table `tblgalleryphotos`
--
ALTER TABLE `tblgalleryphotos`
  ADD PRIMARY KEY (`galleryPhotoId`);

--
-- Indexes for table `tblphototags`
--
ALTER TABLE `tblphototags`
  ADD PRIMARY KEY (`photoTagsID`);

--
-- Indexes for table `tbltags`
--
ALTER TABLE `tbltags`
  ADD PRIMARY KEY (`tagID`),
  ADD UNIQUE KEY `tagDescription` (`tagDescription`);

--
-- Indexes for table `thumbnails`
--
ALTER TABLE `thumbnails`
  ADD PRIMARY KEY (`thumbnailID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblgalleries`
--
ALTER TABLE `tblgalleries`
  MODIFY `galleryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblgalleryphotos`
--
ALTER TABLE `tblgalleryphotos`
  MODIFY `galleryPhotoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tblphototags`
--
ALTER TABLE `tblphototags`
  MODIFY `photoTagsID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tbltags`
--
ALTER TABLE `tbltags`
  MODIFY `tagID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `users`
--


ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
