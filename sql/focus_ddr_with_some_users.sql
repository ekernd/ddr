-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 31, 2017 at 08:27 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `focus_ddr`
--

-- --------------------------------------------------------

--
-- Table structure for table `files`
--
drop table files;
CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `file_extension` varchar(10) NOT NULL,
  `file_size` int(11) NOT NULL,
  `file_uploaded_by_id` int(11) NOT NULL,
  `file_uploaded_date` date NOT NULL,
  `file_deleted_by_id` int(11) NOT NULL,
  `file_deleted_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--
drop table photos;
CREATE TABLE `photos` (
  `photoID` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `photoDescription` varchar(255) NOT NULL,
  `file_id` int(11) NOT NULL,
  `photo_active` enum('yes','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblgalleries`
--
drop table tblgalleries;
CREATE TABLE `tblgalleries` (
  `galleryID` int(11) NOT NULL,
  `galleryDescription` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `photoID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblphototags`
--
drop table tblgalleries;
CREATE TABLE `tblphototags` (
  `photoTagsID` int(11) NOT NULL,
  `tagID` int(11) NOT NULL,
  `photoID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbltags`
--
drop table tbltags;
CREATE TABLE `tbltags` (
  `tagID` int(11) NOT NULL,
  `tagDescription` varchar(50) NOT NULL,
  `tag_active` enum('yes','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `thumbnails`
--
drop table thumbnails;
CREATE TABLE `thumbnails` (
  `thumbnailID` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `file_extension` varchar(10) NOT NULL,
  `file_size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
drop table users;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_first_name` varchar(30) NOT NULL,
  `user_last_name` varchar(30) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` char(32) NOT NULL,
  `user_salt` char(32) NOT NULL,
  `user_role` int(11) NOT NULL DEFAULT '1',
  `user_active` enum('yes','no') NOT NULL DEFAULT 'yes',
  `user_image` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_first_name`, `user_last_name`, `user_email`, `user_password`, `user_salt`, `user_role`, `user_active`, `user_image`) VALUES
(1, 'John', 'Doe', 'john@doe.com', 'opensesame', 'xxx', 1, 'yes', NULL),
(2, 'Jane', 'Doe', 'jane@doe.com', 'letmein', 'xxx', 2, 'yes', NULL),
(3, 'Bob', 'Smith', 'bob@smith.com', 'test', 'xxx', 2, 'yes', NULL),
(4, 'dsfgd', 'sdaf', 'asdf', 'afba40b743d6c7bc420b424c63c6e440', '?5«&b', 1, 'yes', NULL),
(6, 'dsfgdzxcv', 'sdafzxcv', 'asdfzxcv', 'df4ab5adb4c54b80453400d7b037debb', 'žÇ5-Í', 1, 'yes', NULL),
(7, 'dave', 'dave', 'ekernd@gmail.com', '7af72f3447153cb2c2fb5c13da8c3ad3', 'Žjõ%', 1, 'yes', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--
drop table user_roles;
CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL,
  `user_role_name` varchar(30) NOT NULL,
  `user_role_desc` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `user_role_name`, `user_role_desc`) VALUES
(1, 'Standard User', 'Normal user with no special permissions'),
(2, 'Admin', 'Extra permissions');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`photoID`);

--
-- Indexes for table `tblgalleries`
--
ALTER TABLE `tblgalleries`
  ADD PRIMARY KEY (`galleryID`);

--
-- Indexes for table `tblphototags`
--
ALTER TABLE `tblphototags`
  ADD PRIMARY KEY (`photoTagsID`);

--
-- Indexes for table `tbltags`
--
ALTER TABLE `tbltags`
  ADD PRIMARY KEY (`tagID`);

--
-- Indexes for table `thumbnails`
--
ALTER TABLE `thumbnails`
  ADD PRIMARY KEY (`thumbnailID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `photoID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblgalleries`
--
ALTER TABLE `tblgalleries`
  MODIFY `galleryID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblphototags`
--
ALTER TABLE `tblphototags`
  MODIFY `photoTagsID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbltags`
--
ALTER TABLE `tbltags`
  MODIFY `tagID` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
