


DROP TABLE tblgalleryPhotos; 
CREATE TABLE `tblgalleryPhotos` (
   `galleryPhotoId` int(11) NOT NULL,
   `galleryID` int(11) NOT NULL,
   `file_id` int(11) NOT NULL,
   `photoActiveInGallery` enum('yes','no') NOT NULL
    
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tblgalleryPhotos` (`galleryPhotoId`,`galleryID`,`file_id`,`photoActiveInGallery`)
VALUES
(1,1, 1,'yes'),
(2,1, 2,'yes'),
(3,2, 1,'yes'),
(4,3, 1,'yes');